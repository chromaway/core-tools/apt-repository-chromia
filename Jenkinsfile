def tool_url = ''

pipeline {
  agent any

  options {
    timeout(
      time: 1,
      unit: 'HOURS',
    )
  }

  environment {
    NAME = "chr"
    CHR_VERSION = "0.15.2"
    CLI_URL = "https://gitlab.com/api/v4/projects/39844192/packages/maven/com/chromia/cli/chromia-cli/0.15.2/chromia-cli-0.15.2-dist.tar.gz"
    DPKG_COLORS = "always"
  }

  parameters {
    choice(
      name: 'TOOL',
      choices: [
        '-- make a choice --',
        'chr',
        'pmc',
      ],
    )

    string(
      name: 'VERSION',
      defaultValue: '',
      description: 'Version of the tool to deploy, e.g. 1.2.3.',
    )
  }

  stages {
    stage('validate') {
      steps {
        script {
          if (params.TOOL == '-- make a choice --') {
            error('Invalid TOOL parameter.')
          }
        }
      }
    }

    stage('prepare') {
      steps {
        script {
          if (params.TOOL == "chr") {
            tool_url = "https://gitlab.com/api/v4/projects/39844192/packages/maven/com/chromia/cli/chromia-cli/${params.VERSION}/chromia-cli-${params.VERSION}-dist.tar.gz"
          }

          if (params.TOOL == "pmc") {
            tool_url = "https://gitlab.com/api/v4/projects/46346037/packages/maven/net/postchain/mc/management-console/${params.VERSION}/management-console-${params.VERSION}-dist.tar.gz"
          }
        }
      }
    }

    stage('build') {
      steps {
        sh """
          mkdir -p ./package/${params.TOOL}/debian/source
          wget -qO- ${tool_url} | tar xvz -C "./package/${params.TOOL}/debian/source"

          if [ \$? -ne 0 ]; then
              echo "Failed to download from url: ${tool_url}"
              exit 1
          fi


          # Builds debian package from debian directory
          # TODO: make change log include link to release notes
          cd "package/${params.TOOL}"
          echo "${params.TOOL} (${params.VERSION}) stable;

          * Release

          " > debian/changelog
          dpkg-buildpackage -uc -tc -us -rfakeroot
          cd ../..

          # Move .deb packge into apt-repo
          mkdir -p ./apt-repo/pool/main
          cp "./package/${TOOL}_${VERSION}_amd64.deb" ./apt-repo/pool/main/

          # Clean up build
          find ./package -maxdepth 1 -name "${params.TOOL}*" -type f -delete
          rm -r "./package/${TOOL}/debian/source"

          # Download current Package files and appends to it with new package
          cd apt-repo
          ls
          mkdir -p dists/stable/main/binary-amd64
          wget https://apt.chromia.com/dists/stable/main/binary-amd64/Packages -O ./dists/stable/main/binary-amd64/Packages
          dpkg-scanpackages --multiversion --arch amd64 pool/ >> dists/stable/main/binary-amd64/Packages

          # Create and sign Release file
          cd dists/stable
          ../../../generate-release.sh > Release
          gpg --batch --no-tty -abs -u ECF4F9E5C0814D03768EB2A8F25A1AD4EE147F04 -o Release.gpg Release
        """

        withCredentials([
          string(credentialsId: 'AWS_ACCESS_KEY_ID', variable: 'AWS_ACCESS_KEY_ID'),
          string(credentialsId: 'AWS_SECRET_ACCESS_KEY', variable: 'AWS_SECRET_ACCESS_KEY'),
        ]) {
          sh """
            cd ${env.WORKSPACE}/apt-repo

            aws s3 sync --no-progress . s3://apt-repository-chromia/
          """
        }
      }
    }
  }

  post {
    always {
      sh """
        git clean -fd -x
      """
    }
  }
}
