#!/bin/bash

TOOL="chr"
VERSION="0.20.12"
tool_url="https://gitlab.com/api/v4/projects/39844192/packages/maven/com/chromia/cli/chromia-cli/${VERSION}/chromia-cli-${VERSION}-dist.tar.gz"

mkdir -p ./package/${TOOL}/debian/source
wget -qO- ${tool_url} | tar xvz -C "./package/${TOOL}/debian/source"

if [ \$? -ne 0 ]; then
    echo "Failed to download from url: ${tool_url}"
    exit 1
fi


# Sets new version to be greater to be higher than latest version that is
# released up on apt-repository. This to verify we have installed from local apt-repo by 
# running 'chr --version'
NEW_VERSION="0.99.0"
cd "package/${TOOL}"
echo "${TOOL} (${NEW_VERSION}) stable;

* Release

" > debian/changelog
dpkg-buildpackage -uc -tc -us -rfakeroot
cd ../..


# Move .deb packge into apt-repo
mkdir -p ./apt-repo/pool/main
cp "./package/${TOOL}_${NEW_VERSION}_amd64.deb" ./apt-repo/pool/main/

# Clean up build
find ./package -maxdepth 1 -name "${TOOL}*" -type f -delete
rm -r "./package/${TOOL}/debian/source"

# Download current Package files and appends to it with new package
cd apt-repo
# ls
mkdir -p dists/stable/main/binary-amd64
wget https://apt.chromia.com/dists/stable/main/binary-amd64/Packages -O ./dists/stable/main/binary-amd64/Packages


#Scan packages and build apt repo
dpkg-scanpackages --multiversion --arch amd64 pool/ >> dists/stable/main/binary-amd64/Packages

# Create and sign Release file
cd dists/stable
../../../generate-release.sh > Release
gpg --batch --no-tty -abs -u ECF4F9E5C0814D03768EB2A8F25A1AD4EE147F04 -o Release.gpg Release