# Chromia Apt Repository
This repository distributes Chromia CLI (chr) and Postchain Management Console (pmc) to Linux through the apt-get package manager.
It does so through a Jenkins job that handles creating new releases. 

## New release
### Automated release
Normally the Jenkins job is triggered automatically from the [Chromia CLI](https://gitlab.com/chromaway/core-tools/chromia-cli) and 
[Management Console](https://gitlab.com/chromaway/core-tools/management-console) repositories in their respective release
pipelines.

### Manual release
To publish a new version manually of either of the packages the following steps are required:
1. Go to the jenkins job
   https://jenkins.infra.chromia.dev/job/apt-repository-chromia/
2. Click Build with Parameters. For the TOOL parameter select if you are doing a release for chr or pmc. 
   Then write the version (1.2.3) corresponding to the version of the package you want to publish.

## Install through apt-get
1. Download the public key and add it to your local keyrings
    ```bash
    curl -fsSL https://apt.chromia.com/chromia.gpg | sudo tee /usr/share/keyrings/chromia.gpg
    ```
    <br>
2. Add the repository to your list of package sources <br>
   
    ```bash
    echo "deb [arch=amd64 signed-by=/usr/share/keyrings/chromia.gpg] https://apt.chromia.com stable main" | sudo tee /etc/apt/sources.list.d/chromia.list
    ```
3. Update apt-get
    ```bash
    sudo apt-get update
    ```

4. Install package
    ```bash
    sudo apt-get install chr/pmc
    ```


## Running apt repo locally
### Building
You can set up and run a local APT repository for testing purposes by using the `build-apt-repo.sh` script. This script pulls down the required tool, packages it, and configures it in a local APT repository folder structure.

#### Configurable Parameters
At the beginning of the build-apt-repo.sh script, you can configure the following parameters:

* TOOL: Specify the tool to package locally. This can be either chr or pmc.
* VERSION: Set the version of the tool to pull down from the registry.
* TOOL_URL: Specify the URL of the registry to download the tool from.

#### Example Usage
```bash
./build-apt-repo.sh
```

The script will:

Use the apt-repo files located in the package directory to build and package the selected tool.
Reference files such as chr.install, chr.links, rules, and control to manage the packaging process can be found in `package` folder under each tool.

### Hosting
After building the repository, you can host it on a local server using Python's built-in HTTP server (or any hosting service of your choice). Run the following command from the repository's directory:

```bash
python3 -m http.server
```
This will start a server accessible at: http://0.0.0.0:8000/

Add the Local Repository to APT Sources: To enable your system to install packages from the local repository, you need to add the local host to APT's list of sources. Run the following command:

```bash
echo "deb [arch=amd64] http://127.0.0.1:8000/apt-repo stable main" | sudo tee /etc/apt/sources.list.d/example.list
```

Update APT and Allow Insecure Repositories: Since this is a local and unverified source, you will need to allow insecure repositories when updating APT:

```bash
sudo apt-get update --allow-insecure-repositories
```

Install the Package: Now, you can install the package with apt-get:

```bash
sudo apt-get install <tool>
```
Replace <tool> with either chr or pmc depending on what you built.